import z3

from dopefish import constraints, ConstraintsFactory
from dopefish.dopetypes import DopeString
from dopefish.dopetypes.dopeset import DopeStringSet
from anomalysearch.constraints_tree_visitor import ConstraintsTreeVisitor

interfaces = """
interface Entity {
  abstract id: str;
}

[angine=entity]
interface UrlEntity <: Entity {
  path: str;
  level: int;
  tags: [str];
}

[angine=subject]
interface Subject <: Entity {
  name: str;
  roles: [str];
  level: int;
  tags: [str];
  abstract ip: str;
}
"""

policy_pattern_1 = """ {
            apply """
policy_pattern_2 = """
            rule r11 {
                deny
                target clause subject.ip == "192.168.10.5" and ("admin" in subject.roles or "root" in subject.roles)
            }
            rule r12 {
                permit
                target clause subject.ip == "192.168.10.5" and "root" in subject.roles
            }
            rule r13 {
                permit
                target clause subject.ip == "192.168.10.5" and ("user" in subject.roles or "admin" in subject.roles and not "root" in subject.roles)
            }
            rule r14 {
                permit
                target clause subject.ip == "127.0.0.1" or subject.ip == "192.168.10.5"
            }
            rule r15 {
                permit
                target clause subject.ip == "127.0.0.1" or subject.ip == "10.0.10.5"
            }
        }"""

policy = """
namespace example {
    export policySet mainPolicy {
        apply denyUnlessPermit
        policy testFA""" + policy_pattern_1 + "firstApplicable\n" + policy_pattern_2 +\
        "\npolicy testPO" + policy_pattern_1 + "permitOverrides\n" + policy_pattern_2 +\
        "\npolicy testDO" + policy_pattern_1 + "denyOverrides\n" + policy_pattern_2 +\
        "\npolicy testPUD" + policy_pattern_1 + "permitUnlessDeny\n" + policy_pattern_2 +\
        "\npolicy testDUP" + policy_pattern_1 + "denyUnlessPermit\n" + policy_pattern_2 +\
        "\npolicy testOOA" + policy_pattern_1 + "onlyOneApplicable\n" + policy_pattern_2 +\
"""
    }
}
"""

if __name__ == "__main__":
    policy_constraints = ConstraintsFactory.build_tree(
        alfa_policy=policy,
        idl_interfaces=interfaces
    )
    # Extract the first policy (set) from the namespace
    constraint = policy_constraints.policies[0]
    assert isinstance(constraint, constraints.PolicySet)
    # Base policy constraints
    solver = z3.Solver()
    solver.add(constraint.permit)
    solver.add(z3.Not(constraint.deny))
    solver.add(z3.Not(constraint.not_applicable))
    solver.add(z3.Not(constraint.indeterminate))
    solver.add(policy_constraints.additional_constraints)

    # Search anomaly
    visitor = ConstraintsTreeVisitor()
    visitor.visitPolicySet(constraint)
    #visitor.target_comparator.solver.add(z3.String("subject.ip") != z3.StringVal("127.0.0.1"))
    #print('-'*30)
    #visitor.visitPolicySet(constraint)

    # Additional constraints
    # 1. subject.roles should be subset of ["guest", "user", "admin"]
    #roles = policy_constraints.attributes["subject.roles"]
    #allowed_roles = DopeStringSet.from_values(["guest", "user", "admin"])
    #solver.add(roles.issubseteq(allowed_roles).get_constraint())
    #solver.add(allowed_roles.additional_constraints)
    # 2. Subject can't have `admin` rights
    #solver.add(roles.contains(DopeString("admin")).get_constraint(False))

    # Solver should say us that we can get PERMIT using localhost IP with any role.
    #assert solver.check() == z3.sat
    #model = solver.model()
    #for k,v in policy_constraints.attributes.items():
        #print("{} == {}".format(k , model[v.inner_z3var]))

    # And this is the only possible way.
    #solver.add(z3.String("subject.ip") != z3.StringVal("127.0.0.1"))
    #assert solver.check() == z3.unsat


