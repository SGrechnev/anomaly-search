import z3
from .target_comparator import TargetComparator
from dopefish.constraints import Policy, Effect
import copy
import itertools

class ConstraintsTreeVisitor:
    def __init__(self):
        self.target_comparator = TargetComparator()
    
    def fill_row(self, rule1, rules, table):
        """ With chain reaction """
        consequents = []
        for rule2 in rules:
            if rule1 is not rule2 and self.target_comparator.is_implies_rule_targets(rule1, rule2):
                consequents.append(rule2)
        table[rule1] = consequents
        if consequents:
            for rule in consequents:
                if table[rule] is None:
                    self.fill_row(rule, consequents + [rule1], table)
        return consequents
    
    def createG(self, rules):
        if len(rules) < 2:
            return None
        table = { ri: None for ri in rules }
        try:
            while True:
                rule = next(rule for rule in table.keys() if table[rule] is None)
                self.fill_row(rule, rules, table)
        except StopIteration:
            pass
        return table
    
    def createC(self, rules, tableG):
        if len(rules) < 2:
            return None
        #permit_rules = set(filter(lambda rule: rule.effect == Effect.PERMIT, rules))
        #deny_rules = set(rules) - permit_rules
        #print("PERMIT RULES: ", end = '')
        #for rule in permit_rules:
            #print(rule.name, end = ' ')
        #print("\nDENY RULES: ", end = '')
        #for rule in deny_rules:
            #print(rule.name, end = ' ')
        #print('')
        table = { ri: [] for ri in rules }
        for idx, rule1 in enumerate(rules):
            for rule2 in set(rules[idx+1:]) - set(tableG[rule1]):
                if rule1.effect != rule2.effect and self.target_comparator.is_proper_joint_rule_targets(rule1, rule2):
                    table[rule1].append(rule2)
        return table
        
    def createR(self, rules, tableReducedG):
        if len(rules) < 2:
            return None
        table = { rule: [] for rule in rules}
        for idx, rule1 in enumerate(rules):
            for rule2 in set(rules[idx+1:]) - set(tableReducedG[rule1]):
                if rule1.effect == rule2.effect and self.target_comparator.is_joint_rule_targets(rule1, rule2):
                    table[rule1].append(rule2)
        return table
    
    def visitPolicy(self, policy, indent = ''):
        print(indent+'Visit "' + policy.name + '" Policy')
        algorithm_name = policy.algorithm.__name__
        if(algorithm_name == 'OnlyOneApplicable'):
            print(indent+"OnlyOneApplicable algorithm: skipping this policy...")
            return

        tableG = self.createG(policy.rules)
        #print('\n'+'='*10 + ' tableG ' + '='*10)
        #for rule in tableG:
            #print(rule.name, list(map(lambda rule: rule.name, tableG[rule])))

        tableC = self.createC(policy.rules, tableG)

        tableReducedG = dict([i for i in map(lambda rule1: (rule1,list(filter(lambda rule2: rule2.effect != rule1.effect, tableG[rule1]))), tableG)])
        #print('\n'+'#'*10 + ' tableReducedG ' + '#'*10)
        #for rule in tableReducedG:
            #print(rule.name, list(map(lambda rule: rule.name, tableReducedG[rule])))
        tableR = self.createR(policy.rules, tableReducedG)

        print(indent+'#'*3, algorithm_name)
        if algorithm_name == 'FirstApplicable':
            generalMap = lambda rule1: (rule1,list(filter(lambda rule2: policy.rules.index(rule1) < policy.rules.index(rule2), tableReducedG[rule1])))
            shadowingMap = lambda rule1: (rule1,list(filter(lambda rule2: policy.rules.index(rule2) < policy.rules.index(rule1), tableReducedG[rule1])))
        elif algorithm_name == 'PermitOverrides' or algorithm_name == 'DenyUnlessPermit':
            generalMap = lambda rule: (rule, [] if rule.effect != Effect.PERMIT else tableReducedG[rule])
            shadowingMap = lambda rule1: (rule1,list(filter(lambda rule2: rule2.effect == Effect.PERMIT, tableReducedG[rule1])))
        elif algorithm_name == 'DenyOverrides' or algorithm_name == 'PermitUnlessDeny':
            generalMap = lambda rule: (rule, [] if rule.effect != Effect.DENY else tableReducedG[rule])
            shadowingMap = lambda rule1: (rule1,list(filter(lambda rule2: rule2.effect == Effect.DENY, tableReducedG[rule1])))
        else:
            print('Unknown combining algorithm:', algorithm_name)
            return
        tableGeneral = dict([i for i in map(generalMap, tableReducedG)])
        tableShadowing = dict([i for i in map(shadowingMap, tableReducedG)])

        tableLeftHeaderLen = len('-'*10+' tableShadowing '+'-'*10)
        print('\n'+indent+ '-'*10+' tableShadowing '+'-'*10, end ='\t\t')
        print('-'*10 + ' tableGeneralizing ' + '-'*10)
        for rule in policy.rules:
            left_len = len(rule.name+repr(list(map(lambda rule: rule.name, tableShadowing[rule]))))+1
            print(indent+rule.name,\
                list(map(lambda rule: rule.name, tableShadowing[rule])),\
                ' '*(tableLeftHeaderLen-left_len)+'\t\t',\
                list(map(lambda rule: rule.name, tableGeneral[rule])))

        tableLeftHeaderLen = len('-'*10+' tableCorrelated '+'-'*10)
        print('\n'+indent+ '-'*10+' tableCorrelated '+'-'*10, end ='\t\t')
        print('-'*10 + ' tableRedudancy ' + '-'*10)
        for rule in policy.rules:
            left_len = len(rule.name+repr(list(map(lambda rule: rule.name, tableC[rule]))))+1
            print(indent+rule.name,\
                list(map(lambda rule: rule.name, tableC[rule])),\
                ' '*(tableLeftHeaderLen-left_len)+'\t\t',\
                list(map(lambda rule: rule.name, tableR[rule])))
        print('\n\n')
        """print('\n'+indent+'-'*10 + ' tableShadowing ' + '-'*10)
        for rule in tableShadowing:
            print(indent+rule.name, list(map(lambda rule: rule.name, tableShadowing[rule])))

        print('\n'+indent+'-'*10 + ' tableGeneral ' + '-'*10)
        for rule in tableGeneral:
            print(indent+rule.name, list(map(lambda rule: rule.name, tableGeneral[rule])))

        print('\n'+indent+'-'*10 + ' tableCorrelated ' + '-'*10)
        for rule in tableC:
            print(indent+rule.name, ':',list(map(lambda rule: rule.name, tableC[rule])))
        
        print('\n'+indent+'-'*10 + ' tableRedudancy ' + '-'*10)
        for rule in tableR:
            print(indent+rule.name, ':',list(map(lambda rule: rule.name, tableR[rule])))"""

    def visitPolicySet(self, policy_set, indent = ''):
        print(indent+'Visit "' + policy_set.name + '" PolicySet')
        #print(indent+'targets: ', end = '')
        #print(policy_set.targets)
        for inner_ps in policy_set.policy_sets:
            self.visitPolicySet(inner_ps, indent+'\t')
        for inner_policy in policy_set.policies:
            self.visitPolicy(inner_policy, indent+'\t')
        #partition = self.split(self.target_comparator.is_eq_policy_targets, policy_set.policies + policy_set.policy_sets)
        #res = self.find_consequents(self.target_comparator.is_implies_policy_targets, partition)
        #self.__printResult(res, indent='\t')
    
    @staticmethod
    def __printResult(result, indent = ''):
        for item in result:
            print(indent, end='')
            print(list(map(lambda rule: rule.name, item["rules"])), end = '')
            if item["consequents"]:
                print(' =>', list(map(lambda part: list(map(lambda rule: rule.name, part)), item["consequents"])))
            else:
                print('')

