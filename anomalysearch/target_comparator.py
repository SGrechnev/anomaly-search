from typing import Union
import z3
from dopefish.dopetypes import DopeBool
from dopefish.constraints import Rule, Policy, PolicySet

class TargetComparator:
    def __init__(self):
        self.solver = z3.Solver()
    
    #def is_eq_targets(self, target1: DopeBool, target2: DopeBool) -> bool:
        #self.solver.push()
        #self.solver.add( ( target1 == target2 ).is_false() )
        #result = self.solver.check() == z3.unsat
        #self.solver.pop()
        #return result
    
    def is_implies_targets(self, target1: DopeBool, target2: DopeBool) -> bool:
        self.solver.push()
        self.solver.add( ( target1 & ~target2 ).is_true() )
        result = self.solver.check() == z3.unsat
        self.solver.pop()
        return result
    
    def is_proper_joint_targets(self, target1: DopeBool, target2: DopeBool) -> bool:
        self.solver.push()
        self.solver.add(( target1 & target2 ).is_true())
        result = self.solver.check() != z3.unsat
        self.solver.pop()
        if result:
            self.solver.push()
            self.solver.add(( ~target1 & target2 ).is_true())
            result = result and self.solver.check() != z3.unsat
            self.solver.pop()
        return result
    
    def is_joint_targets(self, target1: DopeBool, target2: DopeBool) -> bool:
        self.solver.push()
        self.solver.add(( target1 & target2 ).is_true())
        result = self.solver.check() != z3.unsat
        self.solver.pop()
        return result
    
    def is_implies_rule_targets(self, rule1: Rule, rule2: Rule):
        return self.is_implies_targets(rule1._target_expression, rule2._target_expression)
    
    def is_proper_joint_rule_targets(self, rule1: Rule, rule2: Rule):
        return self.is_joint_targets(rule1._target_expression, rule2._target_expression)
    
    def is_joint_rule_targets(self, rule1: Rule, rule2: Rule):
        return self.is_joint_targets(rule1._target_expression, rule2._target_expression)
    
    def is_implies_policy_targets(self, policy1: Union[Policy, PolicySet], policy2: Union[Policy, PolicySet]):
        return self.is_implies_targets(policy1.target_result, policy2.target_result)
    
    #def is_joint_policy_targets(self, policy1: Union[Policy, PolicySet], policy2: Union[Policy, PolicySet]):
        #return self.is_disjoint_targets(policy1.target_result, policy2.target_result)

