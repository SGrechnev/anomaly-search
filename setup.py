from setuptools import setup, find_packages

setup(
    name="anomaly-search",
    version=0.1,
    description="AlfaScript anomaly search based on ALFA-C",
    author="sgrechnev",
    author_email="sgrechnev@gmail.com",
    license="GPL",
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=open("requirements.txt").readlines(),
)